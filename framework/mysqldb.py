from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text

import config

class mysqlDB():
    # def __init__(self) -> None:
    #     # pass
    #     app.config['SQLALCHEMY_DATABASE_URI'] = f"mysql+pymysql://{config.USERNAME}:{config.PASSWORT}@{
    #         config.HOSTNAME}:{config.PORT}/{config.DATEBASE}?charset=utf8mb4"
    #     #读取app.config的数据库信息
    #     db = SQLAlchemy(app)
    def mydb(app):
            app.config['SQLALCHEMY_DATABASE_URI'] = f"mysql+pymysql://{config.USERNAME}:{config.PASSWORT}@{
            config.HOSTNAME}:{config.PORT}/{config.DATEBASE}?charset=utf8mb4"
            #读取app.config的数据库信息
            db = SQLAlchemy(app)
            return db
    # with app.app_context():
    #     with db.engine.connect() as conn:
    #         res = conn.execute(text('select 1')) 
    #         print('res',res.fetchone())