#!/usr/bin/env python
# encoding: utf-8
def status(code, msg):
    return {'code': code, 'msg': msg}

SUCCESS=status(200, 'Success')
TOKEN_TIME_OUT=status(401, '会话超时')
PARAMETER_ERROR=status(422, '参数错误')
AUTH_FAILED=status(403, '认证失败')
URL_ERROR=status(404, '请求地址不存在')
SERVICE_ERROR=status(500, '服务器内部错误')
