import bcrypt
import hmac
from flask import Response
import json

def write_json(data):
    json_data =  Response(json.dumps(data, ensure_ascii=False), content_type='application/json;charset=utf-8')
    return json_data

def encryp_password(password):
    """密码加密"""
    hashed = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
    return hashed

def verify_password(password, hashed):
    """密码验证"""
    # Validating a hash (don't use ==)
    if (hmac.compare_digest(bcrypt.hashpw(password.encode('utf-8'), hashed), hashed)):
        # Login successful
        return True
    else:
        return False
