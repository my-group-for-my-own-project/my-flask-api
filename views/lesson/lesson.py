# -*- coding:utf-8 -*-
from flask import Blueprint,g,request
from config import *
from base.utils import *
# from framework import my_jwt 
# from flask_jwt_extended import create_access_token,get_jwt_identity,jwt_required
from framework.response_code import *

app_lesson = Blueprint('lesson',__name__,url_prefix='/lesson')


@app_lesson.route('/info',methods=['GET','POST'])
def lessonInfo():
    sql_str = 'SELECT name,pic_url as url,level,price,num_of_people as people,sell_num as join_num FROM lesson WHERE status="True"'
    sql_res = g.db.select(sql_str)
    if sql_res :
        # print(sql_res)
        return {'code':200,'data':sql_res}
    return {'code':201,'msg':'查询失败'}
    