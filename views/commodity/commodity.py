# -*- coding:utf-8 -*-

from flask import Blueprint,g,request,jsonify
from config import *
from base.utils import *
# from framework import my_jwt 
from flask_jwt_extended import jwt_required
# from framework.response_code import *
from base.upload import do_upload

app_commodity = Blueprint('commodity',__name__,url_prefix='/commodity')

# 商品信息获取
@app_commodity.route('/info',methods=['GET'])
@jwt_required()
def commodityInfo():
    sql_str = 'SELECT id,name,creattime,url,brand,details FROM commodity'
    res = g.db.select(sql_str)
    if res:
        return jsonify(res),200
    # return jsonify('服务器内部错误'),500
    return '服务器内部错误',500

# 上传商品图片
@app_commodity.route('/upload',methods=['POST'])
# @jwt_required()
def commodityPhotoUpload():
    # 文件上传的文件信息
    file = request.files['file']
    # 文件上传附带修改参数，用于修改对应的图片地址
    data = json.loads(request.form['data'])
    # do_upload 上传成功返回图片路径 失败返回false
    upLoadMsg = do_upload(file,data)
    # print(upLoadMsg)
    if not upLoadMsg:
        return '上传失败，请重试',500
    return jsonify({'url':upLoadMsg}),200
    # pass
    