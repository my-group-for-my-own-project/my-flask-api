# -*- coding:utf-8 -*-
from flask import Flask,g,jsonify
#设置请求cors
from flask_cors import CORS
#  token jwt库
from flask_jwt_extended import JWTManager
# 时间库
import datetime

# pywsgi.WSGIServer flask的一个web库 一个服务器库 暂了解
from gevent import pywsgi

# 一个库  用于简单部署api服务 好像和蓝图模式有点冲突 现在是用蓝图模式
# from flask_restful import Resource, Api

# 自定义配置 这里写成了config 应该写到myqaldb里面 查资料的时候跟着做了没改过来
# 已经改了 很方便就改了
from db.mysql import SQLManager

# 引入配置
import config

# 引入自定义的接口蓝图  
from views.user import user
from views.commodity import commodity
from views.lesson import lesson

app = Flask(__name__)

#跨域允许
CORS(app,origins='*')


# 注册蓝图
app.register_blueprint(user.app_user)
app.register_blueprint(commodity.app_commodity)
app.register_blueprint(lesson.app_lesson)
# app.config.from_object(__name__)

##处理返回中文为unicode 乱码问题
#为flask2.2以下版本使用
# app.config['JSON_AS_ASCII'] = False 
app.json.ensure_ascii = False

# 设置上传文件的保存路径
app.config['UPLOAD_FOLDER'] = './static/upload'

#jwt_token 配置
#允许token传入的位置
app.config["JWT_TOKEN_LOCATION"] = ["headers", "cookies", "json", "query_string"]
#token有效时间
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(minutes=60*24*15)
# token密匙
app.config['JWT_SECRET_KEY'] = 'my_secret_key_alalalalala'
jwt = JWTManager(app)
# 设置自定义token过期 返回信息
@jwt.expired_token_loader
def my_expired_token_callback(jwt_header, jwt_payload):
    """返回 flask Response 格式"""
    return jsonify(code="401", err="token 已过期"), 401


@app.before_request
def before_request():
    g.db = SQLManager()
    


if __name__ == '__main__':
    app.debug = config.DEBUG #开启debug模式 1.项目修改可以自动重启动  2.出bug可以在浏览器上看到错误信息
    #本地项目h5项目 为了真机测试 需要在wifi端口运行才能访问 
    # app.run(debug=False,port='8000',host='192.168.0.102')
    serve = pywsgi.WSGIServer((config.HOST,config.PORT),app)
    serve.serve_forever()